class String
  def self._origin(name)
    alias_method "_origin_#{name}", name
  end
  private_class_method :_origin

  # TODO: unpack
  # _origin :unpack

  # TODO: upto
  # _origin :upto

  _origin :to_i
  def to_i(base = 0x10)
    _origin_to_i(base)
  end

  # Treats leading characters from str as a string of decimal digits
  # (with an optional sign) and returns the corresponding number.
  # Zero is returned on error.
  def dec
    to_i(0xa)
  end
end
