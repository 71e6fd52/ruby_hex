class Integer
  def self._origin(name)
    alias_method "_origin_#{name}", name
  end
  private_class_method :_origin

  _origin :to_s
  def to_s(base = 0x10)
    _origin_to_s(base)
  end

  _origin :digits
  def digits(base = 0x10)
    _origin_digits(base)
  end

  _origin :inspect
  def inspect(base: 0x10)
    per =
      case base
      when 0x2
        '0b'
      when 0x10
        '0x'
      when 0x8
        '0'
      when 0xa
        ''
      else
        raise ArgumentError, "base #{base} can't inspect"
      end
    "#{'-' if negative?}#{per}#{abs.to_s(base)}"
  end

  _origin :ceil
  def ceil(ndigits = 0, base: 0x10)
    -(-self).floor(ndigits, base: base)
  end

  _origin :floor
  def floor(ndigits = 0, base: 0x10)
    return self if ndigits >= 0

    ndigits = -ndigits.to_i
    self - self % base**ndigits
  end

  _origin :truncate
  def truncate(ndigits = 0, base: 0x10)
    (negative? ? -0x1 : 0x1) * abs.floor(ndigits, base: base)
  end

  _origin :round
  def round(ndigits = 0, half: :up, base: 0x10)
    return self if ndigits >= 0

    ndigits = ndigits.to_i
    half = :up if half.nil?
    n = self % base**-ndigits / base**(-ndigits - 0x1)
    case n <=> (base / 0x2)
    when -1
      floor(ndigits, base: base)
    when 1
      ceil(ndigits, base: base)
    when 0
      case half
      when :up
        (negative? ? -1 : 1) * abs.ceil(ndigits, base: base)
      when :down
        truncate(ndigits, base: base)
      when :even
        fl = floor(ndigits, base: base)
        if (fl % base**(-ndigits + 0x1) / base**-ndigits).even?
          fl
        else
          ceil(ndigits, base: base)
        end
      else
        raise ArgumentError, "invalid rounding mode: #{half}"
      end
    end
  end
end
